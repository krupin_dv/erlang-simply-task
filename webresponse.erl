-module(webresponse).
-export([echo/2, calc_fib/2, factorial/1, fib/1]).
-export([calc/2]).
       

    
    
% ������ �� �������    
% c(webresponse). webresponse:calc_fib(1,2).


% ������ �� ��������
% http://localhost/cgi/webresponse:echo
% http://localhost/cgi/webresponse:calc_fib




echo(_Env, _Input) ->
    "echo".

    
    
% ��������� �������, ������� ������ ������� ��� ����������
calc_fib(_Env, _Input) -> 
    [{1, Res1}, {2, Res2}] = calc(start, 2),
    %[Res1, Res2] = calc(start, 2),
    
    "Result1: " ++ integer_to_list( Res1 ) ++ " Result2: " ++ integer_to_list( Res2 ).

    

 
   
calc(start, N) ->
    Pid1 = spawn(fun() -> start_eval(1) end),
    Pid1 ! {self(), (fun fib/1), 40},
    Pid2 = spawn(fun() -> start_eval(2) end),
    Pid2 ! {self(), (fun fib/1), 39},
    
    calc([], N);

    
    
calc(List, 0) -> 
    %io:format("end: ~p~n", [List]),
    List2 = lists:sort(fun sort_fun/2, List),
    %io:format("List2: ~p~n", [List2]),
    List2;
    
    
    
% List - ������ � �����������, N - ������� ����������� ������ ����    
calc(List, N) ->
    receive
        {Pid, N, Result} ->
            NewList = [{N, Result} | List],
            %io:format("List: ~p, NewList: ~p~n", [List, NewList]),
            %io:format("N: ~p~n", [N]),
            calc(NewList, (N-1))
    end.
    


    

    
% ��������� ������� ���������� Fun � ���������� Arg, ��������� �������� �����
start_eval(N) ->
    receive
        {From, Fun, Arg} ->
            Result = Fun(Arg),
            %io:format("From: ~p~n", [From]),
            %io:format("Result: ~p~n", [Result]),
            From ! {self(), N, Result}

    end.
    
    

% ������� ����������    
sort_fun({A, Res1}, {B, Res2}) ->
    case (A >= B) of
        true -> false;
        false -> true
    end.
    
    
factorial(1) -> 1;
factorial(N) ->
    N * factorial(N - 1).
    
% ����� ���������
fib(0) -> 0;
fib(1) -> 1;
fib(N) -> fib(N-1) + fib(N-2).

    

    
    